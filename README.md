# Contame back server - Magioz
=======================


[![pipeline status](https://gitlab.com/LuuPesado/contame-back-server-magioz/badges/master/pipeline.svg)](https://gitlab.com/LuuPesado/contame-back-server-magioz/commits/master)

[![coverage report](https://gitlab.com/LuuPesado/contame-back-server-magioz/badges/master/coverage.svg)](https://gitlab.com/LuuPesado/contame-back-server-magioz/-/commits/master)


## Introducción
Proyecto creado para el tp inicial de la materia Taller de Desarrollo de proyectos 2 . FIUBA

El enunciado puede encontrarse [acá](https://drive.google.com/drive/u/1/folders/15eJ6TjcA9n9O9IyFFmjgcQUyppHhUwVt)

## Instalación Local

 ### Requisitos
  - Python >= 3.6.X (recomendamos utilizar [virtualenv](https://medium.com/@aaditya.chhabra/virtualenv-with-virtualenvwrapper-on-ubuntu-34850ab9e765))

 ### Python

 Para instalar el código y sus dependencias

 ```
 pip install -r requirements.txt
 or  
 pip3 install -r requirements.txt
 ```


## Ejecucion

Para ejecutarlo localmente existe una entrada de `Makefile` (recomendado ya que corre los test también)

```make run```

o en su defecto

```gunicorn 'app:get_flask_app()'```

Tambien se puede correr gunicorn con la opcion `--reload` para que tome automaticamente los cambios en los source files. Si la aplicacion muere (por ejemplo syntax error al cargar), tenes que correr el comando de nuevo igual.

Esto localmente iniciará un servidor escuchando en 

`http://localhost:8000`

## .env

Se necesita crear un archivo .env con las siguientes variables de ambiente:

```
MONGODB_URI = 'mongodb://localhost:27017/cuentameDB'
OAUTHLIB_RELAX_TOKEN_SCOPE=true
#PERMITE HTTP
OAUTHLIB_INSECURE_TRANSPORT=true
GOOGLE_OAUTH_CLIENT_ID= setear el google clien id aqui
ADMIN_EMAIL= email que tiene permitido ser admin
FIREBASE_TOKEN = 

```

## CICD

Para asegurate que no vaya a fallar el pipeline, deberas correr los tests y flask8 por consola antes de commitear

```
flake8 --max-line-length=150 *.py
pytest
```

Cada vez que se haga un push a master, se hara un deploy de la app a heroku: https://contame-magioz-back-qa.herokuapp.com/

## Mongo DB
Antes que nada deberas asegurarte de tener instalado mongo
```
sudo apt-get install mongodb
```

Para crear la base de datos local deberás correr:
```
python3 tools/setup_db.py 
```
Si hay problemas con el setup el estilo o de connection refused
```
Traceback (most recent call last):
  File "tools/setup_db.py", line 9, in <module>
    list_of_db = client.list_database_names()
```
Verificar que tengas mongo instalado, y que el servicio este corriendo `sudo service mongodb status`


Para borrar la base de datos local deberás correr:
```
python3 tools/drop_db.py 
```

## Documentación

Para acceder a la documentación de la api debemos pegarle con el path `/api/doc`  

En caso de querer modificarla dirigirse al archivo `/doc/api.yaml`. (Swagger v3)

