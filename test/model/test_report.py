import unittest
from model.report import Report
from mongoengine.errors import ValidationError


class TestReport(unittest.TestCase):
    def test_create_report(self):
        rep_data = {
            "image_url": ["http://firebase.fake.addres.com"],
            "address": "Avenida la Plata 243, CABA",
            "category": "Autos abandonados",
            "report_type": "Autos abandonados",
            "details": "Auto abandonado desde hace 3 semanas",
            "priority": "Baja",
            "votes": 3,
            "state_internal": "Inspección",
            "state_public": "Aceptado",
            "barrio": "Palermo",
            "comuna": "14"

        }
        myReport = Report(**rep_data)
        myReport.validate()
        self.assertIsInstance(myReport, Report)

    def test_image_url_raises_exception_when_given_nonurl_string(self):
        rep_data = {
            "image_url": ["not_an_url"],
            "address": "Avenida la Plata 243, CABA",
            "category": "Autos abandonados",
            "report_type": "Autos abandonados",
            "details": "Auto abandonado desde hace 3 semanas",
            "priority": "Baja",
            "votes": 3,
            "state_internal": "Inspección",
            "state_public": "Aceptado"
        }
        brokenReport = Report(**rep_data)
        with self.assertRaises(ValidationError):
            brokenReport.validate()
