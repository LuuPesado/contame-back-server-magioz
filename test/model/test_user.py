import unittest
from model.user import User
from mongoengine.errors import ValidationError


class TestReport(unittest.TestCase):
    def test_create_report(self):
        user_data = {
            'user_id': '1234567890',
            'name': 'Juan',
            'familyName': 'Perez',
            'email': 'juanperez@gmail.com',
            'photoUrl': 'https://lh3.googleusercontent.com/'
        }
        myUser = User(**user_data)
        myUser.validate()
        self.assertIsInstance(myUser, User)
        print(myUser.to_json_for_list())
        expected_json = {'id': '1234567890',
                         'name': 'Juan',
                         'familyName': 'Perez',
                         'email': 'juanperez@gmail.com',
                         'photoUrl': 'https://lh3.googleusercontent.com/'
                         }
        self.assertEqual(expected_json, myUser.to_json_for_list())

    def test_image_url_raises_exception_when_given_nonurl_string(self):
        user_data = {
            'user_id': '1234567890',
            'name': 'Juan',
            'familyName': 'Perez',
            'email': 'juanperez@gmail.com',
            'photoUrl': 'not an url'
        }
        brokenUser = User(**user_data)
        with self.assertRaises(ValidationError):
            brokenUser.validate()
