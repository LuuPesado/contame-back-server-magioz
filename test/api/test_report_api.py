from app import get_flask_app, db
import unittest
import json


class TestReportsApi(unittest.TestCase):

    def setUp(self):
        # Test client
        self.test_app = get_flask_app().test_client()
        # Drop report collection before all tests
        db.report.drop()

    def test_post_report(self):
        rep_data = {
            "address": "Avenida la Plata 243, CABA",
            "category": "Autos abandonados",
            "report_type": "Autos abandonados",
            "details": "Auto abandonado desde hace 3 semanas",
            "image_url": ["http://firebase.fake.addres.com"],
            "priority": "Baja",
            "votes": 3,
            "state_internal": "Inspección",
            "state_public": "Aceptado",
            "barrio": "Palermo",
            "comuna": "14"
        }
        r = self.test_app.post(
            '/reports', data=json.dumps(rep_data), content_type='application/json')
        self.assertEqual(r.status_code, 200)

    def test_get_reports(self):
        r = self.test_app.get('/reports')
        self.assertEqual(r.status_code, 200)
