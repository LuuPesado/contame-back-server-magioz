from app import get_flask_app, db
import unittest
import json

# project resources
from model.user import User
from model.report import Report


class TestUserApi(unittest.TestCase):

    def setUp(self):
        # Test client
        self.test_app = get_flask_app().test_client()
        # Drop report collection before all tests
        db.user.drop()

    def test_post_user(self):
        user_data = {
            'user_id': '1234567890',
            'name': 'Juan',
            'familyName': 'Perez',
            'email': 'juanperez@gmail.com',
            'photoUrl': 'https://i.pinimg.com/originals/de/64/80/de64801f0275c1ab2ea5a9e2bb3ce7bc.jpg'
        }
        r = self.test_app.post('/users', data=json.dumps(user_data),
                               content_type='application/json')
        self.assertEqual(r.status_code, 200)

    def test_get_user(self):
        user_data = {
            'user_id': '1234567890',
            'name': 'Juan',
            'familyName': 'Perez',
            'email': 'juanperez@gmail.com',
            'photoUrl': 'https://i.pinimg.com/originals/de/64/80/de64801f0275c1ab2ea5a9e2bb3ce7bc.jpg'
        }
        r = self.test_app.post('/users', data=json.dumps(user_data),
                               content_type='application/json')
        r = self.test_app.get('/user/1234567890')
        self.assertEqual(r.status_code, 200)

    def test_get_users(self):
        user_data = {
            'user_id': '1234567890',
            'name': 'Juan',
            'familyName': 'Perez',
            'email': 'juanperez@gmail.com',
            'photoUrl': 'https://i.pinimg.com/originals/de/64/80/de64801f0275c1ab2ea5a9e2bb3ce7bc.jpg'
        }
        r = self.test_app.post('/users', data=json.dumps(user_data),
                               content_type='application/json')
        r = self.test_app.get('/users')
        self.assertEqual(r.status_code, 200)

    def test_recently_created_user_reports_are_empty(self):
        user_id = '1234567890'
        user_data = {
            'user_id': user_id,
            'name': 'Juan',
            'familyName': 'Perez',
            'email': 'juanperez@gmail.com',
            'photoUrl': 'https://lh3.googleusercontent.com/'
        }
        r = self.test_app.post('/users', data=json.dumps(user_data),
                               content_type='application/json')
        r = self.test_app.get('/user/' + user_id + '/reports')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.get_json()["reports"]), 0)

    def test_get_user_reports_adding_one(self):
        myUser = self.setup_test_user()
        myReport = self.setup_test_report(myUser)
        r = self.test_app.get('/user/' + myUser.user_id + '/reports')

        self.assertEqual(r.status_code, 200)
        user_reports = r.get_json()["reports"]
        self.assertEqual(len(user_reports), 1)
        # Assert that has the correct category
        self.assertEqual(user_reports[0]["category"], myReport.category)
        # Assert that belongs to the user
        self.assertEqual(user_reports[0]["user"]["id"], myUser.user_id)

    def test_user_reports_must_retrieve_invalid_states(self):
        myUser = self.setup_test_user()
        myInvalidReport = self.setup_test_report_with_invalid_state(myUser)
        r = self.test_app.get('/user/' + myUser.user_id + '/reports')
        self.assertEqual(r.status_code, 200)
        user_reports = r.get_json()["reports"]
        self.assertEqual(len(user_reports), 1)
        self.assertEqual(user_reports[0]["state_public"], myInvalidReport.state_public)

    def setup_test_report_with_invalid_state(self, myUser):
        rep_data = {
            "image_url": ["http://firebase.fake.addres.com"],
            "address": "Avenida la Plata 243, CABA",
            "category": "Autos abandonados",
            "report_type": "Autos abandonados",
            "details": "Auto abandonado desde hace 3 semanas",
            "priority": "Baja",
            "votes": 3,
            "state_internal": "Inspección",
            "state_public": "Inválido",
            "user": myUser
        }
        myReport = Report(**rep_data)
        myReport.save()
        return myReport

    def setup_test_report(self, myUser):
        rep_data = {
            "image_url": ["http://firebase.fake.addres.com"],
            "address": "Avenida la Plata 243, CABA",
            "category": "Autos abandonados",
            "report_type": "Autos abandonados",
            "details": "Auto abandonado desde hace 3 semanas",
            "priority": "Baja",
            "votes": 3,
            "state_internal": "Inspección",
            "state_public": "Aceptado",
            "user": myUser
        }
        myReport = Report(**rep_data)
        myReport.save()
        return myReport

    def setup_test_user(self):
        user_id = '1234567890'
        user_data = {
            'user_id': user_id,
            'name': 'Juan',
            'familyName': 'Perez',
            'email': 'juanperez@gmail.com',
            'photoUrl': 'https://lh3.googleusercontent.com/'
        }
        myUser = User(**user_data)
        myUser.save()
        return myUser
