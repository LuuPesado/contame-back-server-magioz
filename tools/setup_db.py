# import MongoClient
from pymongo import MongoClient
from decouple import config

# Creating a client
client = MongoClient(config('MONGODB_URI'))
print('mongo client set at port 27017')
# Greating a database nemed cuentameDB
list_of_db = client.list_database_names()
print('checking db existance in %s', list_of_db)
if config('MONGODB_URI').split('/')[-1] in list_of_db:
    print("Database already Exists !!")
else:
    db = client[config('MONGODB_URI').split('/')[-1]]
    print("Database is created !!")
