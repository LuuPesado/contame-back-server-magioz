# import MongoClient
from pymongo import MongoClient
from decouple import config

# Creating a client
client = MongoClient(config('MONGODB_URI'))
print('mongo client set at port 27017')
# Greating a database nemed cuentameDB
list_of_db = client.list_database_names()
print('checking db existance in %s', list_of_db)

if config('MONGODB_URI').split('/')[-1] in list_of_db:
    db = client[config('MONGODB_URI').split('/')[-1]]
    collections = (
        db[c] for c in db.list_collection_names()
        if not c.startswith('system.')
    )
    for collection in collections:
        db.drop_collection(collection)
