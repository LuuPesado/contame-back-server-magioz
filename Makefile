run:
	gunicorn 'app:get_flask_app()'
flake_test:
	flake8 --max-line-length=150 *.py --count --statistics
tests:
	pytest
.PHONY: run, flake_test
