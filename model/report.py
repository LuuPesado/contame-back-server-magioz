from mongoengine import DynamicDocument, EmbeddedDocument
from mongoengine import DateTimeField, SequenceField, URLField, StringField
from mongoengine import IntField, EmbeddedDocumentField, ListField, FloatField, ReferenceField
import datetime
from model.user import User
from flask import jsonify


class HistoryItem(EmbeddedDocument):
    new_state = StringField()
    old_state = StringField()
    comment = StringField()
    performed_at = DateTimeField(default=datetime.datetime.now)
    user = IntField()  # TODO: Switch to a reference field

    def to_json_for_list(self):
        return {
            'new_state': self.new_state,
            'old_state': self.old_state,
            'comment': self.comment,
            'created_at': self.performed_at.isoformat()
        }


class Vote(EmbeddedDocument):
    comment = StringField()
    performed_at = DateTimeField(default=datetime.datetime.now)
    user = ReferenceField(User)

    def to_json_for_list(self):
        return {
            'user': self.user.to_json_for_list(),
            'performed_at': self.performed_at.isoformat(),
            'comment': self.comment
        }


class Report(DynamicDocument):
    STARTING_REPORT_ID = 4321
    report_id = SequenceField(
        value_decorator=lambda x: x + Report.STARTING_REPORT_ID)
    created_at = DateTimeField(default=datetime.datetime.now)
    image_url = ListField(URLField(), required=True)
    address = StringField(required=True)
    category = StringField()
    report_type = StringField()
    details = StringField(max_length=1000)
    priority = StringField(default='Sin asignar')
    state_internal = StringField(default='Ingresado')
    state_public = StringField(default='Ingresado')
    state_internal_history = ListField(EmbeddedDocumentField(HistoryItem))
    state_public_history = ListField(EmbeddedDocumentField(HistoryItem))
    lat = FloatField()
    lon = FloatField()
    user_fcm_token = StringField()  # TODO: move to user
    user = ReferenceField(User)
    barrio = StringField()
    comuna = StringField()
    area = StringField(default='Sin Asignar')
    expediente = StringField(default='Sin Asignar')
    vote_list = ListField(EmbeddedDocumentField(Vote))

    def to_json_for_list(self):
        votes_list = []
        user = ''
        # find a way to get aggregation to de reference fields normally, this instance thing is annoying
        if isinstance(self.user, str):
            user = User.objects(id=self.user).get().to_json_for_list()
            for vote in self.vote_list:
                votes_list.append(Vote(**vote).to_json_for_list())
        elif isinstance(self.user, User):
            user = self.user.to_json_for_list()
            for vote in self.vote_list:
                votes_list.append(vote.to_json_for_list())
        return {
            'id': self.report_id,
            'image_url': self.image_url,
            'address': self.address,
            'category': self.category,
            'report_type': self.report_type,
            'created_at': self.created_at.isoformat(),
            'details': self.details,
            'priority': self.priority,
            'votes': len(self.vote_list),
            'votes_list': votes_list,
            'state_public': self.state_public,
            'state_internal': self.state_internal,
            'lat': self.lat,
            'lon': self.lon,
            'user_fcm_token': self.user_fcm_token,
            'user': user,
            'barrio': self.barrio,
            'comuna': self.comuna,
            'area': self.area,
            'expediente': self.expediente
        }

    def to_json_for_detail(self):
        public_history_list = [history.to_json_for_list() for history in self.state_public_history]
        private_history_list = [history.to_json_for_list() for history in self.state_internal_history]
        init_status = self.init_status()

        state_history_attributes = {
            'state_public_history': init_status + public_history_list,
            'state_internal_history': init_status + private_history_list
        }
        return {**self.to_json_for_list(), **state_history_attributes}

    def init_status(self):
        return [{
            'new_state': 'Ingresado',
            'old_state': '',
            'comment': 'Reporte ingresado al sistema.',
            'created_at': self.created_at.isoformat()
        }]
