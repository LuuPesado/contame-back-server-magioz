from mongoengine import DynamicDocument
from mongoengine import URLField, StringField


class User(DynamicDocument):
    user_id = StringField()
    name = StringField(default='Desconocido')
    familyName = StringField(default='Desconocido')
    email = StringField(default='Desconocido')
    photoUrl = URLField()

    def to_json_for_list(self):
        return {
            'id': self.user_id,
            'name': self.name,
            'familyName': self.familyName,
            'email': self.email,
            'photoUrl': self.photoUrl
        }
