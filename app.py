from flask import Flask, app
from flask_restful import Api
from flask_mongoengine import MongoEngine
from flask_cors import CORS
from pymongo import MongoClient
from flask_swagger_ui import get_swaggerui_blueprint
from yaml import Loader, load
import os

# local packages
from api.routes import create_routes

# external packages
from decouple import config

# Config. database and set default.
client = MongoClient(
    config('MONGODB_URI', default='mongodb://localhost:27017/cuentameDB'))
db = client.get_database(config('DB_NAME', default='cuentameDB'))
print(f'Connected to DB: {db.name}')

# Configure api doc on /api/doc path
SWAGGER_URL = '/api/doc'
SWAGGER_FILE = 'doc/api.yaml'
SWAGGER_PATH = os.path.join(os.path.dirname(__file__), SWAGGER_FILE)


def get_flask_app(configuration: dict = None) -> app.Flask:
    """
    Initializes Flask app with given configuration.
    Main entry point for wsgi (gunicorn) server.
    :param config: Configuration dictionary
    :return: app
    """
    # init flask
    flask_app = Flask(__name__)
    # configure app
    flask_app.config['MONGODB_SETTINGS'] = {'host': config('MONGODB_URI',
                                                           default='mongodb://localhost:27017/cuentameDB'),
                                            'db': config('DB_NAME', default='cuentameDB'),
                                            'retryWrites': False}

    # init api and routes
    api = Api(app=flask_app)
    create_routes(api=api)

    CORS(flask_app)
    # init mongoengine
    MongoEngine(app=flask_app)

    # initialize_flask_dance(flask_app)
    initialize_api_documentation(flask_app)
    return flask_app


def initialize_api_documentation(flask_app):
    doc = open(SWAGGER_PATH, 'r')
    swagger_yml = load(doc, Loader=Loader)
    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL, '/doc/api.yaml', config={'spec': swagger_yml})
    flask_app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)
    doc.close()


if __name__ == '__main__':
    # Main entry point when run in stand-alone mode.
    app = get_flask_app()
    app.run(debug=True, host='0.0.0.0')
