
# flask packages
from flask_restful import Api
# project resources
from api.resources.report import ReportApi, ReportsApi
from api.resources.reportVote import ReportVote
from api.resources.user import UserApi, UsersApi
from api.resources.userReports import UserReportsApi
from api.resources.reportHistory import ReportHistoryPublic, ReportHistoryInternal
from api.resources.statesReport import StatesReport
from api.resources.heatMapData import HeatMapData
from api.resources.timeAverages import TimeAverages
from api.resources.login import Login


def create_routes(api: Api):
    """Adds resources to the api.
    :param api: Flask-RESTful Api Object
    """
    api.add_resource(ReportsApi, '/reports')
    api.add_resource(ReportApi, '/report/<report_id>')
    api.add_resource(ReportVote, '/report/<report_id>/vote')
    api.add_resource(ReportHistoryPublic, '/report/<report_id>/publicState')
    api.add_resource(ReportHistoryInternal,
                     '/report/<report_id>/internalState')
    api.add_resource(UsersApi, '/users')
    api.add_resource(UserApi, '/user/<user_id>')
    api.add_resource(Login, '/login')
    api.add_resource(UserReportsApi, '/user/<user_id>/reports')
    api.add_resource(StatesReport, '/graphs/states')
    api.add_resource(HeatMapData, '/graphs/heatMap')
    api.add_resource(TimeAverages, '/graphs/timeAverages')
