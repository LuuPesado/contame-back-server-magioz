from flask import Response, jsonify, abort
from flask_restful import Resource, request
from model.report import Report, Vote
from model.user import User
from mongoengine import DoesNotExist 


class ReportVote(Resource):
    USER_KEY = 'user'

    def put(self, report_id: str) -> Response:
        data = request.get_json()
        report = Report.objects(report_id=int(report_id) - Report.STARTING_REPORT_ID).get()
        user_id = str(data[self.USER_KEY])
        try:
            user = User.objects(user_id=user_id).get()
        except DoesNotExist:
            abort(400, description='User not found')
        data[self.USER_KEY] = user
        if any(vote.user.user_id == user_id for vote in report.vote_list):
            abort(400, description='User has already voted') 
        report.vote_list.append(Vote(**data))
        put_report = report.save()
        return jsonify({'result': put_report})

