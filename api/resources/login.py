from flask import Response, jsonify
from flask_restful import Resource
from api.authentication import authenticate


class Login(Resource):
    @authenticate
    def get(self) -> Response:
        return jsonify({'result': 'Login exitoso!'})
