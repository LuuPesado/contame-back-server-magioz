from flask import Response, jsonify
from flask_restful import Resource

# project resources
from model.user import User
from model.report import Report


class UserReportsApi(Resource):
    """
    Flask-resftul resource for returning db.report collection.
    """
    def get(self, user_id: str) -> Response:
        existing_user = User.objects(user_id=user_id).get()
        reports = list(Report.objects(user=existing_user).order_by('-created_at'))
        json_reports = list(map(lambda report: report.to_json_for_list(), reports))
        return jsonify(reports=json_reports)
