from flask import Response, jsonify
from flask_restful import Resource, reqparse
from model.report import Report
from datetime import datetime
# from api.authentication import authenticate


class HeatMapData(Resource):
    CATEGORY_KEY = 'category'
    DATE_KEY = 'year'
    filter_params = [CATEGORY_KEY, DATE_KEY]
    def parse_get_args(self):
        parser = reqparse.RequestParser()
        for param in self.filter_params:
            parser.add_argument(param, required=True)
        return parser.parse_args()

    # @authenticate
    def get(self) -> Response:
        args = self.parse_get_args()
        from_date = datetime.strptime(args[self.DATE_KEY], "%Y")
        to_date = datetime.strptime(str(int(args[self.DATE_KEY]) + 1), "%Y")
        heat_map_pipline = [
            { "$match": {
                "category": args[self.CATEGORY_KEY],
                "created_at": {"$gt": from_date, "$lt": to_date},
            }},
            { "$project": {
                "lat": True,
                "lng": "$lon",
                "_id": False,
                "barrio": "$barrio",
                "category": "$category",
            }}
        ]
        heat_map_points = list(Report.objects.aggregate(heat_map_pipline))
        return jsonify(heat_map_points)
