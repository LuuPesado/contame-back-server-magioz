from flask import Response, jsonify
from flask_restful import Resource, request
from model.report import Report, HistoryItem
from utils.notification_utils import NotificationUtils

from api.authentication import authenticate

# WARNING abstract class without using ABC, fix this antipattern


class ReportHistory(Resource):
    NEW_STATE_KEY = 'new_state'
    COMMENT_KEY = 'comment'

    def __init__(self):
        self.state_key = 'state_public'
        self.history_key = 'state_public_history'

    def get(self, report_id: str) -> Response:
        # FIXME stop this thing from requiring th subtraction
        report = Report.objects(report_id=int(
            report_id) - Report.STARTING_REPORT_ID).get()
        return jsonify(report[self.history_key])

    @authenticate
    def put(self, report_id: str) -> Response:
        data = request.get_json()
        report = Report.objects(report_id=int(
            report_id) - Report.STARTING_REPORT_ID).get()
        data['old_state'] = report[self.state_key]
        report[self.state_key] = data[self.NEW_STATE_KEY]
        report[self.history_key].append(HistoryItem(**data))
        put_report = report.save()
        return jsonify({'result': put_report})


class ReportHistoryPublic(ReportHistory):
    def __init__(self):
        self.state_key = 'state_public'
        self.history_key = 'state_public_history'

    @authenticate
    def put(self, report_id: str) -> Response:
        result = super().put(report_id)
        # Send notification
        report = Report.objects(report_id=int(
            report_id)-Report.STARTING_REPORT_ID).get()
        if report.user_fcm_token or report.user:
            notif_result = NotificationUtils().send_notification(report)
            if notif_result["success"]:
                print("Se envió una notificación al usuario con token {}".format(
                    report.user_fcm_token))
            else:
                print("No se pudo enviar la notificacion: " + notif_result["results"][0]["error"]) 
        else:
            print("Error: El reporte no tiene token de usuario. No se envió una notificación.")
        return result


class ReportHistoryInternal(ReportHistory):
    decorators = [authenticate]

    def __init__(self):
        self.state_key = 'state_internal'
        self.history_key = 'state_internal_history'
