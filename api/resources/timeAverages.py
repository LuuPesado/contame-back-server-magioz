from flask import Response, jsonify, abort
from flask_restful import Resource, request, reqparse
from model.report import Report
from datetime import datetime
# from api.authentication import authenticate


class TimeAverages(Resource):
    DATE_FORMAT = '%Y-%m-%d'
    FROM_KEY = 'from'
    TO_KEY = 'to'
    filter_params = [FROM_KEY, TO_KEY]
    def parse_get_args(self):
        parser = reqparse.RequestParser()
        for param in self.filter_params:
            parser.add_argument(param)
        return parser.parse_args()

    # @authenticate
    def get(self) -> Response:
        # args = self.parse_get_args()
        # Trying to solve this problem via aggregations finds us in a bit of a mess due to conditional processing
        # so instead will use normal code to attack problem iterating over one document at a time
        args = self.parse_get_args()
        filters = {"stringDate": {"$gte": args[self.FROM_KEY], "$lte": args[self.TO_KEY]}}
        states_times = {
            'Ingresado': { "total_time": 0, "count": 0 },
            'Aceptado': { "total_time": 0, "count": 0 },
            'En proceso': { "total_time": 0, "count": 0 },
            'Pendiente de obra mayor': { "total_time": 0, "count": 0 },
            'Resuelto': { "total_time": 0, "count": 0 },
            'Inválido': { "total_time": 0, "count": 0 },
        }
        #fixate timing before potentially long iteration
        now = datetime.now() 
        timeavgs_pipeline = [
            { "$addFields": {"stringDate": { "$dateToString": { "format": self.DATE_FORMAT, "date": "$created_at", 'timezone': '-03' }}}},
            { "$match": filters},
            {"$project": {"created_at": True, "state_public":True, "state_public_history": True}}
        ]
        #For loop on a cursor works with lazy loading
        for report in Report.objects.aggregate(timeavgs_pipeline):
            if not report["state_public_history"]:
                self.updateTimeFor(
                    state = states_times['Ingresado'],
                    from_time = report["created_at"],
                    to_time = now,
                )
            else:
                # Calculate time spent until first state change
                first_state = report["state_public_history"][0]
                self.updateTimeFor(
                    state = states_times['Ingresado'],
                    from_time =  report["created_at"],
                    to_time = first_state["performed_at"],
                )
                # Intermediates
                self.calculateIntermediateStateChanges(
                    states_times = states_times,
                    history = report["state_public_history"],
                )
                # Calculate time spent in current step
                curr_state = report["state_public"]
                last_state = report["state_public_history"][-1]
                self.updateTimeFor(
                    state = states_times[curr_state],
                    from_time = last_state["performed_at"],
                    to_time = now,
                )
        for state_key in states_times:
            state = states_times[state_key]
            # in hours
            state['average'] = (state['total_time'] / state['count']) / (60 * 60) if state['count'] > 0 else 0
        return jsonify(states_times)


    def calculateIntermediateStateChanges(self, history, states_times):
        state_changes = iter(history)
        # Save and skip first state, we are going to remember previous one and calculate time difference to current
        current_state = next(state_changes)
        for state_change in state_changes:
            self.updateTimeFor(states_times[current_state['new_state']], current_state["performed_at"], state_change['performed_at'])
            current_state = state_change


    def updateTimeFor(self, state, from_time, to_time):
        # dont worry about overflow python can handle VERY big ints
        state['total_time'] += int((to_time - from_time).total_seconds())
        state['count'] += 1 