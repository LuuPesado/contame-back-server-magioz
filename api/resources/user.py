from flask import Response, jsonify
from flask_restful import Resource, request, reqparse

# from flask_jwt_extended import jwt_required, get_jwt_identity

# project resources
from model.user import User


class UsersApi(Resource):
    """
    Flask-resftul resource for returning db.report collection.
    """
    """QS = QueryString"""
    PAGE_SIZE_QS_KEY = 'pageSize'
    PAGE_NUMBER_QS_KEY = 'pageNumber'
    ORDER_BY_QS_KEY = 'sort_by'
    SORT_ORDER_QS_KEY = 'sort_order'
    filter_params = ['name', 'familyName', 'email']

    PAGE_SIZE_DEFAULT = 10
    PAGE_NUMBER_DEFAULT = 0
    ORDER_BY_DEFAULT = 'created_at'

    def parse_get_args(self):
        parser = reqparse.RequestParser()
        parser.add_argument(self.PAGE_SIZE_QS_KEY, type=int,
                            default=self.PAGE_SIZE_DEFAULT, help='Page size debe ser un int')
        parser.add_argument(self.PAGE_NUMBER_QS_KEY, type=int,
                            default=self.PAGE_NUMBER_DEFAULT, help='Page number debe ser un int')
        parser.add_argument(self.ORDER_BY_QS_KEY,
                            default=self.ORDER_BY_DEFAULT)
        parser.add_argument(self.SORT_ORDER_QS_KEY)
        for param in self.filter_params:
            parser.add_argument(param)
        # TODO check if this sanitizes or we are very vulnerable to injection
        return parser.parse_args()

    def get(self) -> Response:
        """
        GET response method for single documents in report collection.
        :return: JSON object
        """
        args = self.parse_get_args()
        page_size = args[self.PAGE_SIZE_QS_KEY]
        page_number = args[self.PAGE_NUMBER_QS_KEY]
        order_by = args[self.ORDER_BY_QS_KEY]
        sort_order = args[self.SORT_ORDER_QS_KEY]
        filters = {}
        for param in self.filter_params:
            if args[param] is not None:
                filters[param] = args[param]
        # https://docs.mongodb.com/manual/aggregation/
        sort_and_filter_pipeline = [
            {"$project": {"_id": False}},
            {"$match": filters},
            {"$sort": {
                order_by: 1 if sort_order == 'asc' else -1,
            }},
        ]
        count_pipeline = sort_and_filter_pipeline + [
            {"$count": 'documentsCount'}
        ]
        count_result = list(User.objects.aggregate(count_pipeline))
        total_users = count_result[0]['documentsCount'] if count_result else 0
        result_pipeline = sort_and_filter_pipeline + [
            {"$skip": page_number * page_size},
            {"$limit": page_size},
        ]
        users = list(User.objects.aggregate(result_pipeline))
        json_users = list(map(lambda report: User(
            **report).to_json_for_list(), users))
        return jsonify(total=total_users, users=json_users)

    def post(self) -> Response:
        """
        POST response method for creating report.
        :return: JSON object
        """
        data = request.get_json(force=True)
        print(data)
        existing_user = User.objects(user_id=data['user_id'])
        if not existing_user:
            post_user = User(**data).save()
            output = post_user.user_id
        else:
            output = existing_user.get()["user_id"]
        return jsonify({'result': {'id': str(output)}})



class UserApi(Resource):
    """
    Flask-resftul resource for returning db.report collection.
    """

    def get(self, user_id) -> Response:
        """
        GET response method for single documents in report collection.
        :return: JSON object
        """
        output = User.objects(user_id=user_id).get()
        return jsonify({'result': output})
