from flask import Response, jsonify
from flask_restful import Resource, request, reqparse
# project resources
from model.report import Report
from model.user import User

from api.authentication import authenticate


class ReportsApi(Resource):
    """
    Flask-resftul resource for returning db.report collection.
    """

    """QS = QueryString"""
    PAGE_SIZE_QS_KEY = 'pageSize'
    PAGE_NUMBER_QS_KEY = 'pageNumber'
    ORDER_BY_QS_KEY = 'sort_by'
    SORT_ORDER_QS_KEY = 'sort_order'
    filter_params = ['barrio', 'comuna', 'category', 'priority', 'state_internal', 'state_public', 'report_type']

    PAGE_SIZE_DEFAULT = 10
    PAGE_NUMBER_DEFAULT = 0
    ORDER_BY_DEFAULT = 'created_at'

    def parse_get_args(self):
        parser = reqparse.RequestParser()
        parser.add_argument(self.PAGE_SIZE_QS_KEY, type=int,
                            default=self.PAGE_SIZE_DEFAULT, help='Page size debe ser un int')
        parser.add_argument(self.PAGE_NUMBER_QS_KEY, type=int,
                            default=self.PAGE_NUMBER_DEFAULT, help='Page number debe ser un int')
        parser.add_argument(self.ORDER_BY_QS_KEY,
                            default=self.ORDER_BY_DEFAULT)
        parser.add_argument(self.SORT_ORDER_QS_KEY)
        for param in self.filter_params:
            parser.add_argument(param)
        # TODO check if this sanitizes or we are very vulnerable to injection
        return parser.parse_args()

    def get(self) -> Response:
        """
        GET response method for all documents in report collection.
        :return: JSON object
        """
        args = self.parse_get_args()
        page_size = args[self.PAGE_SIZE_QS_KEY]
        page_number = args[self.PAGE_NUMBER_QS_KEY]
        order_by = args[self.ORDER_BY_QS_KEY]
        sort_order = args[self.SORT_ORDER_QS_KEY]
        filters = {}
        for param in self.filter_params:
            if args[param] is not None:
                filters[param] = args[param]
        # https://docs.mongodb.com/manual/aggregation/
        sort_and_filter_pipeline = [
            {"$project": {"_id": False}},
            {"$addFields": {"votes": {"$size": "$vote_list"}}},
            {"$match": filters},
            {"$sort": {
                order_by: 1 if sort_order == 'asc' else -1,
            }},
        ]
        count_pipeline = sort_and_filter_pipeline + [
            {"$count": 'documentsCount'}
        ]
        count_result = list(Report.objects.aggregate(count_pipeline))
        total_reports = count_result[0]['documentsCount'] if count_result else 0
        result_pipeline = sort_and_filter_pipeline + [
            {"$skip": page_number * page_size},
            {"$limit": page_size},
        ]
        reports = list(Report.objects.aggregate(result_pipeline))
        json_reports = list(map(lambda report: Report(
            **report).to_json_for_list(), reports))
        return jsonify(total=total_reports, reports=json_reports)

    def post(self) -> Response:
        """
        POST response method for creating report.
        :return: JSON object
        """
        data = request.get_json(force=True)
        print(data)
        if "user" in data:
            user = User.objects(user_id=data['user']).get()
            del data['user']
            data['user'] = user
        post_report = Report(**data).save()
        output = {'id': str(post_report.report_id)}
        return jsonify({'result': output})


class ReportApi(Resource):
    """
    Flask-resftul resource for returning db.report collection.
    """

    def get(self, report_id: str) -> Response:
        """
        GET response method for single documents in report collection.
        :return: JSON object
        """
        output = Report.objects(report_id=int(
            report_id)-Report.STARTING_REPORT_ID).get()
        return jsonify({'result': output.to_json_for_detail()})

    @authenticate
    def put(self, report_id: str) -> Response:
        """
        PUT response method for updating a report.
        :return: JSON object
        """
        data = request.get_json()
        # Fix this hack to get by report id
        report = Report.objects(report_id=int(
            report_id)-Report.STARTING_REPORT_ID).get()
        modifiable_attributes = ['address', 'category', 'details', 'image_url', 'area', 'expediente',
                                 'priority', 'report_type', 'state_internal', 'state_public', 'votes']
        for x in modifiable_attributes:
            if x in data:
                report[x] = data[x]
        put_report = report.save()
        return jsonify({'result': put_report})
