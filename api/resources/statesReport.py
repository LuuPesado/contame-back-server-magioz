from flask import Response, jsonify, abort
from flask_restful import Resource, request, reqparse
from model.report import Report
from datetime import datetime, timedelta
# from api.authentication import authenticate


class StatesReport(Resource):
    DATE_FORMAT = '%Y-%m-%d'
    FROM_KEY = 'from'
    TO_KEY = 'to'
    NEIGHBOURHOOD_KEY = 'barrio'
    filter_params = [FROM_KEY, TO_KEY, NEIGHBOURHOOD_KEY]
    def parse_get_args(self):
        parser = reqparse.RequestParser()
        for param in self.filter_params:
            parser.add_argument(param)
        return parser.parse_args()

    # @authenticate
    def get(self) -> Response:
        args = self.parse_get_args()
        filters = {"stringDate": {"$gte": args[self.FROM_KEY], "$lte": args[self.TO_KEY]}}
        if self.NEIGHBOURHOOD_KEY in args and args[self.NEIGHBOURHOOD_KEY]:
            filters["barrio"] = args[self.NEIGHBOURHOOD_KEY]
        state_pipline = [
            { "$addFields": {"stringDate": { "$dateToString": { "format": self.DATE_FORMAT, "date": "$created_at", "timezone": "-03" }}}},
            { "$match": filters},
            { "$group": { # Group categories and states
                "_id": {
                    "state": "$state_public",
                    "category": "$category",
                },
                "categoryCount": {"$sum": 1}
            }},
            { "$group": { # We now group by states for each category
                "_id": "$_id.state",
                "categories": { 
                    "$push": { 
                        "category": "$_id.category",
                        "count": "$categoryCount"
                    },
                },
                "count": {"$sum": "$categoryCount"}
            }},
            { "$project": {
                "state": "$_id",
                "categories": True,
                "total": "$count",
                "_id": False
            }}
        ]
        states = list(Report.objects.aggregate(state_pipline))
        return jsonify(states)

