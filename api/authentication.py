from flask import request
from flask_restful import abort
from functools import wraps
from google.oauth2 import id_token
from google.auth.transport import requests
from decouple import config


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not getattr(func, 'authenticated', True):
            return func(*args, **kwargs)
        try:
            token = request.headers['Authorization']
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), config("GOOGLE_OAUTH_CLIENT_ID"))
            print("You are {userId} on Google".format(userId=idinfo['email']))
            if idinfo['email'] not in config("ADMIN_EMAIL").split(","):
                raise ValueError("Unauthorized email")
        except ValueError as err:
            print('Google token validation failed: ', err)
            abort(401, description='Tu cuenta no tiene acceso a este servicio')
        except KeyError:
            abort(401, description='No estas loggueado')

        return func(*args, **kwargs)
    return wrapper
