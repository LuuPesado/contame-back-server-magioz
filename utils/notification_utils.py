
from pyfcm import FCMNotification
from decouple import config


class NotificationUtils:
    def send_notification(self, report):
        push_service = FCMNotification(
            api_key=config('FIREBASE_TOKEN'))
        registration_id = report.user_fcm_token
        message_title = "Actualización de reporte #{}".format(report.report_id)
        # Get the last history item
        lastHistoryItem = report.state_public_history[-1]
        message_body = "El reporte cambió el estado de '{}' a '{}'".format(
            lastHistoryItem['old_state'], lastHistoryItem['new_state'])
        message_payload = {"report_id": report.report_id}
        result = push_service.notify_single_device(
            registration_id=registration_id, message_title=message_title,
            message_body=message_body, data_message=message_payload)
        return result
